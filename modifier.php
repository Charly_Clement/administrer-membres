<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>MODIFIER</title>
</head>

<body class="m-5">

        <?php

        try {

            $servername = '127.0.0.1';
            $username = 'charly787';
            $password = 'MaYa/2019';
            $db = 'gestion';

            $pdo = new PDO("mysql:host=$servername;dbname=$db;", $username, $password);
            $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        }
        catch(PDOException $e) {
            echo "Erreur :" . $e->getMessage();
        }

        ?>
    
    <div class="container-fluid">

        <h1 class="mt-5"><b>Administrer les membres</b></h1>
        <h3>Modifier les informations de <?php echo $_SESSION['pseudo']; ?></h3><br/>

        <form method="POST">
            <div class="d-flex justify-content-around">
                <input type="text"   name="nom"    value="<?php echo $_SESSION['nom']    ?>" placeholder="Nom">
                <input type="text"   name="prenom" value="<?php echo $_SESSION['prenom'] ?>" placeholder="Prénom">
                <input type="text"   name="mail"   value="<?php echo $_SESSION['mail']   ?>" placeholder="Email">
                <input type="text"   name="pseudo" value="<?php echo $_SESSION['pseudo'] ?>" placeholder="Pseudo">
                <input type="submit" name="submit" value="Modifier" class="btn btn-primary font-weight-bold px-5">
            </div>
        </form><br>

    </div>

    <div>
        <h3>Journal des modifications</h3>
    </div>


    <?php

        $nom    = isset($_POST['nom'])    && !empty($_POST['nom'])    ? $_POST['nom']    : '';
        $prenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom'] : '';
        $mail   = isset($_POST['mail'])   && !empty($_POST['mail'])   ? $_POST['mail']   : '';
        $pseudo = isset($_POST['pseudo']) && !empty($_POST['pseudo']) ? $_POST['pseudo'] : '';
        $submit = isset($_POST['submit']);
        $date   = date('d/m/Y à H:i');

        if ($submit) {
            try {  
                
                $req = $pdo->prepare("SELECT * FROM membres WHERE pseudo = :pseudo");
                $req ->execute(['pseudo' => $_SESSION['pseudo']]);
                $results = $req -> fetchAll();
                $id = $results[0]['id_membres'];

                $req = $pdo->prepare("UPDATE membres SET nom=:nom,prenom=:prenom,mail=:mail,pseudo=:pseudo WHERE id_membre=".$_GET['id'] );
                $req -> execute(['nom' => $nom,'prenom' => $prenom,'mail' => $mail,'pseudo' => $pseudo,'id_membre' => $_GET['id']]);

                $req = $pdo->prepare("INSERT INTO modification (date_modif,id_pseudo)
                                    VALUES (:date_modif,:id_pseudo)");
                $req -> execute(['date_modif' => $date, 'id_pseudo' =>$id]);
                
            }
            catch(PDOException $e) {
                echo "Erreur :" . $e->getMessage();
            }
        }
        
      
        $req = $pdo->prepare("SELECT * FROM membres");
        $req ->execute();
        $results = $req -> fetchAll();
        

        echo '<center><table class="w-100 border border-primary mt-2 p-3 text-center">
            <tr>
                <th class="pb-3">Id</th>
                <th class="pb-3">Date</th>
                <th class="pb-3">Pseudo</th>
            </tr>';
        
        foreach ($results as $membre) {
            echo '<tr>
                    <td class="p-2">'.$membre['id_membres']   .'</td>
                    <td class="p-2"> </td>
                    <td class="p-2">'.$membre['pseudo']   .'</td>
                </tr>';
        }
            echo '</table></center>';


    ?>

    <br>

    <div class="text-center">
        <a href="gestion.php" class="btn btn-primary font-weight-bold px-3">Revenir à la gestion</a>
    </div>



    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>