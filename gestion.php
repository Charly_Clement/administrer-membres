<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Administer members</title>
</head>

<body class="m-5">
    
    <?php

        try {

            $servername = '127.0.0.1';
            $username = 'charly787';
            $password = 'MaYa/2019';
            $db = 'gestion';

            $pdo = new PDO("mysql:host=$servername;dbname=$db;", $username, $password);
            $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           
        }
        catch(PDOException $e) {
            echo "Erreur :" . $e->getMessage();
        }

    ?>

    <div class="container-fluid">

        <h1 class="mt-5"><b>Administrer les membres</b></h1>
        <h3>Ajouter un membre</h3><br/>
        
        <form method="POST">
            <div class="d-flex justify-content-around">
                <input type="text"   name="nom"    placeholder="Nom">
                <input type="text"   name="prenom" placeholder="Prénom">
                <input type="text"   name="mail"   placeholder="Email">
                <input type="text"   name="pseudo" placeholder="Pseudo">
                <input type="submit" name="submit" value="Ajouter" class="btn btn-primary font-weight-bold px-5">
            </div>
        </form>

    </div>

    
    <?php
    
    $nom    = isset($_POST['nom'])    && !empty($_POST['nom'])    ? $_POST['nom']    : '';
    $prenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom'] : '';
    $mail   = isset($_POST['mail'])   && !empty($_POST['mail'])   ? $_POST['mail']   : '';
    $pseudo = isset($_POST['pseudo']) && !empty($_POST['pseudo']) ? $_POST['pseudo'] : '';
    $submit = isset($_POST['submit']);
    
    if ($submit) {
        try {
            
            $req = $pdo->prepare("INSERT INTO membres (nom,prenom,mail,pseudo) VALUES(:nom,:prenom,:mail,:pseudo)");
            $req -> execute(['nom' => $nom,'prenom' => $prenom,'mail' => $mail,'pseudo' => $pseudo]);
            
        }
        catch(PDOException $e) {
            echo "Erreur :" . $e->getMessage();
        }
    }
    
    $req = $pdo->prepare("SELECT * FROM membres");
    $req ->execute();
    $results = $req -> fetchAll();
    
    echo '<h3 class="mt-5">Il y a ' . COUNT($results) . ' membres inscrit(s)</h3>';

    echo '<center><table class="w-100 border border-primary mt-5 p-3 text-center">
        <tr>
            <th class="pb-3">Nom</th>
            <th class="pb-3">Prénom</th>
            <th class="pb-3">Email</th>
            <th class="pb-3">Pseudo</th>
            <th colspan="2" class="pb-3">Action</th>
        </tr>';
    
    foreach ($results as $membre) {
        echo '<tr>
                <td class="p-2">'.$membre['nom'].'</td>
                <td>'            .$membre['prenom'].'</td>
                <td>'            .$membre['mail']  .'</td>
                <td>'            .$membre['pseudo'].'</td>
                <td>'.'<a href="?modif=ok&pseudo='.$membre['pseudo'].'" class="btn text-decoration-none bg-warning text-white p-1">Modifier</a>'.'</td>
                <td>'.'<a href="?lien=sup&id='.$membre['id_membres'].'" class="btn text-decoration-none bg-danger text-white p-1">Supprimer</a>'.'</td>
            </tr>';
    }
        echo '</table></center>';

        $sup = isset($_GET['sup'])   && !empty($_GET['sup'])   ? $_GET['sup']   : '';
        $id  = isset($_GET['id'])    && !empty($_GET['id'])    ? $_GET['id']    : '';

    if ($sup == 'sup') {
        if ($id != null) {
            $req = $pdo->prepare("DELETE FROM membres WHERE id_membres=?");
            $req -> execute([$id]);
            header('Location:gestion.php');
        }
    }

        $modif   = isset($_GET['modif'])     && !empty($_GET['modif'])      ? $_GET['modif']     : '';
        $pseudo  = isset($_GET['pseudo'])    && !empty($_GET['pseudo'])    ? $_GET['pseudo']    : '';

    if ($modif == 'ok' ) {
        if ($pseudo != null) {
            $req = $pdo->prepare("SELECT * FROM membres WHERE pseudo=?");
            $req -> execute([$pseudo]);
            $results = $req->fetchAll();
            header('Location:modifier.php');
            $_SESSION['nom']    = $results[0]['nom'];
            $_SESSION['prenom'] = $results[0]['prenom'];
            $_SESSION['mail']   = $results[0]['mail'];
            $_SESSION['pseudo'] = $results[0]['pseudo'];
        }
    }

    ?>

















    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>